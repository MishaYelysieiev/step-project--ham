$(document).ready(function () {
    //CHANGING OF THE SURVICE BLOCK CONTENT ON 'CLICK'//
    const serviceText = {
        web_design: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum voluptates, facilis tempora ad architecto, obcaecati maiores iste non laudantium perspiciatis cupiditate impedit dolorem fugiat numquam aliquam. Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis. Excepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error repellat repellendus ratione, et cupiditate quia!",
        graphic_design: "Illum voluptates, facilis tempora ad architecto, obcaecati maiores iste non laudantium perspiciatis cupiditate impedit dolorem fugiat numquam aliquam. Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis. Excepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error",
        online_support: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum voluptates, facilis tempora ad architecto, obcaecati maiores iste non laudantium perspiciatis cupiditate impedit dolorem fugiat numquam aliquam. Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis. Excepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error repellat repellendus ratione, et cupiditate quia!",
        app_design: "Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis. Excepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error repellat repellendus ratione, et cupiditate quia!",
        online_marketing: "Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum voluptates, facilis tempora ad architecto, obcaecati maiores iste non laudantium perspiciatis cupiditate impedit dolorem fugiat numquam aliquam.",
        seo_service: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum voluptatesExcepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error repellat repellendus ratione, et cupiditate quia!"
    };
    $(".service-theme").click((e) => {
        $(".service-theme").removeClass("service-active");
        $(e.target).addClass("service-active");
        let themeKey = $('.service-active').attr('data-theme');
        let textKey = themeKey.split("-").join("_");
        $("#service-img").attr("src", `images/services/service-${themeKey}.jpg`);
        switch (true) {
            case themeKey == "web-design":
                $(".service-content>p").html(serviceText.web_design);
                break;
            case themeKey == "graphic-design":
                $(".service-content>p").html(serviceText.graphic_design);
                break;
            case themeKey == "online-support":
                $(".service-content>p").html(serviceText.online_support);
                break;
            case themeKey == "app-design":
                $(".service-content>p").html(serviceText.app_design);
                break;
            case themeKey == "online-marketing":
                $(".service-content>p").html(serviceText.online_marketing);
                break;
            case themeKey == "seo-service":
                $(".service-content>p").html(serviceText.seo_service);
                break;
        }
    });

    //CREATING OF DATABASE FOR PORTFOLIO BLOCK//
    const graph = "graphic-design";
    const web = "web-design";
    const landing = "landing-pages";
    const wordpress = "wordpress";
    const portfolioHover = {
        graph: "<div class='portfolio-hover'> <div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>awesome design</p><p>graphic design</p></div></div>",
        web: "<div class='portfolio-hover'> <div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p><p>web design</p></div></div>",
        land: "<div class='portfolio-hover'> <div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>variety of patterns</p><p>landing pages</p></div></div>",
        word: "<div class='portfolio-hover'> <div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>functional</p><p>wordpress</p></div></div>"
    };

    //INITIAL ADDING OF THE PORTFOLIO PICTURES//
    for (let i = 1; i <= 3; i++) {
        $(".portfolio").append(`<div class='port-img ${graph}'><img src='images/portfolio/${graph}/${graph}${i}.jpg' alt='web'></div>`);
        $(".portfolio").append(`<div class='port-img ${web}'><img src='images/portfolio/${web}/${web}${i}.jpg' alt='web'></div>`);
        $(".portfolio").append(`<div class='port-img ${landing}'><img src='images/portfolio/${landing}/${landing}${i}.jpg' alt='web'></div>`);
        $(".portfolio").append(`<div class='port-img ${wordpress}'><img src='images/portfolio/${wordpress}/${wordpress}${i}.jpg' alt='web'></div>`);
    }
    getImgHover = () => {
        $(".graphic-design>img").before(portfolioHover.graph);
        $(".web-design>img").before(portfolioHover.web);
        $(".landing-pages>img").before(portfolioHover.land);
        $(".wordpress>img").before(portfolioHover.word);
    }

    //PORTFOLIO BLOCK 'FILTER' EVENT//
    let loadBtnStatus = false;
    let currentClass = false;
    $(".portfolio-theme").click((e) => {
        $(".portfolio-theme").removeClass("portfolio-active");
        $(e.target).addClass("portfolio-active");
        currentClass = $(e.target).attr('data-filter');
        if (currentClass != "all") {
            $(`.${currentClass}`).fadeIn("fast");
            $(".port-img").not(`.${currentClass}`).css("display", "none");
        } else {
            $(".port-img").css("display", "block");
        }
    });

    //PORTFOLIO BLOCK 'LOAD MORE' EVENT//
    $(".portfolio-load").click(() => {
        $(".portfolio-block>.load-wrapp").css("display", "block");
        setTimeout(() => {
            $(".portfolio-block>.load-wrapp").css("display", "none");
            if (loadBtnStatus == false) {
                for (let j = 4; j <= 6; j++) {
                    $(".portfolio").append(`<div class='port-img ${graph}'><img src='images/portfolio/${graph}/${graph}${j}.jpg' alt='web'></div>`);
                    $(".portfolio").append(`<div class='port-img ${web}'><img src='images/portfolio/${web}/${web}${j}.jpg' alt='web'></div>`);
                    $(".portfolio").append(`<div class='port-img ${landing}'><img src='images/portfolio/${landing}/${landing}${j}.jpg' alt='web'></div>`);
                    $(".portfolio").append(`<div class='port-img ${wordpress}'><img src='images/portfolio/${wordpress}/${wordpress}${j}.jpg' alt='web'></div>`);
                    loadBtnStatus = true;
                }
            } else {
                for (let k = 7; k <= 9; k++) {
                    $(".portfolio").append(`<div class='port-img ${graph}'><img src='images/portfolio/${graph}/${graph}${k}.jpg' alt='web'></div>`);
                    $(".portfolio").append(`<div class='port-img ${web}'><img src='images/portfolio/${web}/${web}${k}.jpg' alt='web'></div>`);
                    $(".portfolio").append(`<div class='port-img ${landing}'><img src='images/portfolio/${landing}/${landing}${k}.jpg' alt='web'></div>`);
                    $(".portfolio").append(`<div class='port-img ${wordpress}'><img src='images/portfolio/${wordpress}/${wordpress}${k}.jpg' alt='web'></div>`);
                    $(".portfolio-load").hide();
                }
            }
            if (currentClass != "all" && currentClass != false) {
                $(`.${currentClass}`).css("display", "block");
                $(".port-img").not(`.${currentClass}`).css("display", "none");
            };
            getImgHover();
        }, 2000);
    });
    getImgHover();

    //SLICK SLIDER//
    $('.rev-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.rev-slider-nav',
        autoplay: true,
        autoplaySpeed: 3000,
        cssEase: 'linear'
    });
    $('.rev-slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.rev-slider',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '0',
        cssEase: 'linear'
    });

    //MASONRY//
    $(".gallery-load").click(() => {

        $(".gallery-block>.load-wrapp").css("display", "block");
        setTimeout(() => {
            $(".gallery-block>.load-wrapp").css("display", "none");
            $(".gallery-load").remove();
            for (let i = 10; i < 17; i++) {
                $('.grid').append(`<div class="grid-item grid-hide"><div class="grid-hover"><a href="#"><i class="fas fa-search"></i></a><a href="#"><i class="fas fa-expand"></i></a></div><img src="images/gallery/gal${i}.png" alt=""></div>`)};
            $('.grid').masonry('reloadItems');
            $('.grid').masonry('layout');
            window.setTimeout(function () {
                $('.grid').masonry({
                    itemSelector: '.grid-item',
                    columnWidth: 25,
                    gutter: 10
                });
            }, 500);
        }, 2000);
    });

    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 25,
        gutter: 10
    });
})